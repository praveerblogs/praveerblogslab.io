---
title: About me
subtitle: What I've been up to   
comments: false
---
Hi!

I'm Praveer. I convert logic to artificial written languages (coding) in order to help myself and other people make money.

Currently employed at AJIO.com - working as a backend developer.

### My professional experience:

- Developing supply chain software in IBM Sterling OMS and WMS platform
- Backend java development and a bit of front-end development (nothing fancy - just bootstrap and semantic UI) in spring boot framework.

### What interests me:

- I'm obsessed with music - I think I do everything with music in background, I follow music blogs/vlogs/forums. Do check out my last.fm. I know how to play keyboard, currently learning electric guitar, vocals and music theory.
- I'm also highly interested in complex systems - I hope to write articles about them someday.
- machine learning/artificial intelligence
- Hard/speculative science fiction - I'm a big fan of Neal Stephenson. I was ecstatic after reading Anathem.


